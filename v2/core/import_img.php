
<?php
require_once '../vendor/autoload.php';
ini_set('max_execution_time', 300);

$file = $_FILES["File"];

//ratio px to mm
// $r = 11.8;
$imagedetails = getimagesize($file["tmp_name"]);
$width = $imagedetails[0];
$height = $imagedetails[1];

$photo_to_paste = $file["tmp_name"];
$path = $file['name'];
$ext = pathinfo($path, PATHINFO_EXTENSION);
move_uploaded_file( $file['tmp_name'], "../tmp/img_tmp.".$ext);

$info = array(
    'width' => $width,
    'height' => $height,
    'url' => 'tmp/img_tmp.'.$ext
);

echo json_encode($info);
