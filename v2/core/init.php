<?php
require_once '../vendor/autoload.php';
ini_set('max_execution_time', 300);

$file = $_FILES["File"];
$h = intval($_POST["height"]);
$w = intval($_POST["width"]);
$text = $_POST["text"];
$cookie = $_POST["cookie"];
$im_posx = intval($_POST["im_posx"]);
$im_posy = intval($_POST["im_posy"]);
$im_w = intval($_POST["im_w"]);
$im_h = intval($_POST["im_h"]);


//**************************//
// generate the preview png //
//**************************//

    $photo_to_paste = $file["tmp_name"];

    $img = imagecreatetruecolor($w, $h);
    imagealphablending($img, false);
    $transparency = imagecolorallocatealpha($img, 0, 0, 0, 127);
    imagefill($img, 0, 0, $transparency);
    imagesavealpha($img, true);

    $white = imagecolorallocate($img, 255, 255, 255);
    $black = imagecolorallocate($img, 0, 0, 0);

    $font_path = '../assets/font/Syne-Extra.ttf';
    $fontsize = intval($_POST["fs"]);
    // 0.75 font ratio px to pt
    $textbr = \andrewgjohnson\linebreaks4imagettftext($fontsize*0.75, 0, $font_path, $text, $w);
    imagettftext($img, $fontsize*0.75,0, 0, $fontsize*0.75, $white, $font_path, $textbr);
    // imagescale
    list($width, $height) = getimagesize($photo_to_paste);
    $src = imagecreatefromjpeg($photo_to_paste);
    $im2 = imagecreatetruecolor($im_w, $im_h);
    imagecopyresampled($im2, $src, 0, 0, 0, 0, $im_w, $im_h, $width, $height);

    //todo : ammeliorer le traitement de l'image ici
    imagefilter($im2, IMG_FILTER_GRAYSCALE);
    imagefilter($im2, IMG_FILTER_CONTRAST, -100);


    // copy image onto the canvas
    imagecopy($img, $im2, $im_posx, $im_posy,0, 0,$im_w, $im_h);

    $file = '../tmp/'.$cookie.'.png';

    imagepng($img, $file);
    imagedestroy($img);

    // populate json //
    $stutt_info = array(
        'id' => $cookie,
        'width' => $w,
        'height' => $h,
        'text' => $text,
        'im_posx' => $im_posx,
        'im_posy' => $im_posy,
        'im_h' => $im_h,
        'im_w' => $im_w,
        'font-size' => $_POST["fs"],
        'origin_img' => 'data:image/png;base64,'.base64_encode(file_get_contents($file))
    );

    file_put_contents("../tmp/".$cookie."_stutt-info.json", json_encode($stutt_info));


    ///////////////////


    echo '<div id="stut_pre" style="display:inline-block;width:'.$w.'px;height:'.$h.'px;" id="preview"><img id="pre_img" style="width=100%; height=auto;" src="data:image/png;base64,'.base64_encode(file_get_contents($file)).'" alt="preview error"/></div>';

    unlink($file);
