
<?php
require_once '../vendor/autoload.php';
require_once 'mirage.php';
$cookie = $_POST["cookie"];



$json = json_decode(file_get_contents("../tmp/".$cookie."_stutt-info.json"), true);
$new_pic = mmirage($cookie, $json["origin_img"], $_POST['repeat'], $_POST['bandwidth'], $_POST['offset'], $_POST['gutter'], $_POST['color'], false);
$stutt_data = array(
        "data" => $_POST['color'].";".$_POST['repeat'].";".$_POST['bandwidth'].";".$_POST['offset'].";".$_POST['gutter'],
        "b64" => 'data:image/png;base64,'.$new_pic
);
if( array_key_exists("historic", $json) ){
    $tm = $_POST['timestamp'];
    $json["historic"][$tm] = $stutt_data;
}else{

    $json["historic"] = array($_POST['timestamp'] => $stutt_data) ;
}

file_put_contents("../tmp/".$cookie."_stutt-info.json", json_encode($json));

echo '<div id="stut_pre" style="display:inline-block;width:'.$json["width"].'px;height:'.$json["height"].'px;" id="preview"><img id="pre_img" style="width=100%; height=auto;" src="data:image/png;base64,'. $new_pic .'" alt="preview error"/></div>';

?>
