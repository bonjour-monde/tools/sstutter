<?php
  $cookie_name = "id";
  $cookie_value = randomStr(10);
  if(!isset($_COOKIE[$cookie_name])) {
    setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
    header("Refresh:0");
  }
?>

<!DOCTYPE html>
<html lang="fr" class="">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="Pragma" content="no-cache">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Stuttttter ¬ MMIRAGE</title>
  <script type="text/javascript" src="assets/lib/jq.js"></script>
  <script type="text/javascript" src="assets/lib/txtfill.js"></script>
  <script type="text/javascript" src="assets/lib/spectrum.js"></script>
  <script type="text/javascript" src="assets/script/interface.js"></script>
  <link rel="stylesheet" href="assets/style/mirage.css">
</head>
<body>
<div class="container">
<div class="space"></div>
<h1><span class="title"><i>Stuttttter ¬ </i><strong>MMIRAGE</strong></span></h1>
<div class="space"></div><div class="space"></div>
<form id="doc">
1 — Set your document size (in mm) →
<label for="width">Width:</label>
<input style="display:none;" type="text" name="cookie" value="<?php echo $_COOKIE[$cookie_name]; ?>">
<input id="width" class="size" type="text" name="width" value="800" maxlength="5" size="5">
<label for="height">Height:</label>
<input id="height" class="size" type="text" name="height" value="600" maxlength="5" size="5"><br/>
2 — Import an image → <input id="image" type="file" name="File">
<button id="import" type="button" name="submit">LOAD</button><br/>
3 — Click anywhere in the frame to enter your text
</form>
<div class="space"></div>
<div id="canvas">
    <textarea spellcheck="false"></textarea>
    <div class="image resizable">
        <div class='resizers'>
            <div class='resizer bottom-right'></div>
        </div>
    </div>
</div>
<div class="space"></div>
4 — <button id="init" type="button" name="stutt">START STUTTTTTERING</button><br/>
<div id="zone">
    <form id="stut_form">
        <input style="display:none;" type="text" name="cookie" value="<?php echo $_COOKIE[$cookie_name]; ?>">
        But first, pick a color → <input type='text' name="color" id="colorPick" /><br/><br>
        Then go crazy<br>
        <label class="range-type" for="repeat">Repeat</label>
        <input class="st" type="range" id="repeat" name="repeat" value="10" min="0" max="100">
        <label class="range-type" for="bandwidth">Spread</label>
        <input class="st" type="range" id="bandwidth" name="bandwidth" value="10" min="0" max="100"><br>
        <label class="range-type" for="offset">Offset</label>
        <input class="st" type="range" id="offset" name="offset" value="10" min="0" max="100">
        <label class="range-type" for="gutter">Gutter</label>
        <input class="st" type="range" id="gutter" name="gutter" value="10" min="0" max="100">

    </form><br/>
    <div id="preview">
    </div>
    <br><br>History ↘
    <ul id="historic">
    </ul>
<div class="space"></div>
    5 — Are you done?<br><br>
   Choose your export resolution →
   <select name="dpi" class="select-css">
       <option value="96">96 DPI</option>
       <option value="150">150 DPI</option>
       <option value="300">300 DPI</option>
</select><br><br>

    And finally <button id="export" type="button" name="submit">SAVE AS .PNG</button><br/>
    <span class="result_link"></span>
</div>

<div id="dummy">
    <span></span>
</div>
</div>
</body>
</html>


<?php
//cookie !
function randomStr($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>
