# stttuter ¬ Mmirage (python)

![](../_img/out.gif)

### Prerequisites

the program use these libraries :
* tkinter
* PIL

## Using the program

This program work in command line as well as with an (rough) interface.

### stttuter CLI  

Use this command line :
```bash
python3 stutter-cli.py --crop 12 --offset 34 --repeat 5 myfile.jpg
```

* -c, --crop -> Height of the cropzone (0 to 100%)
* -o, --offset -> Offset from top (0 to 100%)
* -r, --repeat -> Number of repetition (0 to 100%)

### stttuter GUI

Simply use this command line :
```bash
python3 stutter.py
```
