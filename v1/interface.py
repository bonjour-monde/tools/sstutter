# todo :
# export button
# adapt interface
# adapt Scale > ok
# more functions  !
# ew ns sens de découpe


from tkinter import filedialog
from tkinter import *
from PIL import Image, ImageTk

import tkinter as tk
import numpy as np
import PIL.Image, PIL.ImageTk

SIZE = [800,600]

class toolbar:

    def __init__(self, master):
        maincolor = "#AAFFF4"
        shade = "#29B2A1"
        exp_c = "#FFF98C"
        import_c = "#FFF98C"
        self.master = master
        self.frame = tk.Frame(self.master, bg=maincolor)

        # position the slider next to each other
        global slide_height
        slide_height = self.slide_height = tk.Scale(self.frame, from_=0, to=100, orient=VERTICAL, command = self.sstutter, bd=1, background = maincolor,activebackground = exp_c, troughcolor=shade, highlightbackground=maincolor )
        self.slide_offset = Scale(self.frame, from_=0, to=100, orient=VERTICAL, command = self.sstutter, bd=1, background = maincolor, activebackground = exp_c,troughcolor=shade, highlightbackground=maincolor)
        self.slide_repeat = Scale(self.frame, from_=0, to=100, orient=VERTICAL, command = self.sstutter, bd=1, background = maincolor, activebackground = exp_c,troughcolor=shade, highlightbackground=maincolor)

        #import /export
        self.importB = tk.Button(self.frame, text = 'import image', width = 25, background = exp_c, activebackground = shade, highlightbackground=shade, command = self.importImg)
        self.exportB = tk.Button(self.frame, text = 'export image', width = 25,  background = import_c, activebackground = shade, highlightbackground=shade,
        command = self.save)

        self.slide_height.pack(padx=5, pady=10,side=LEFT)
        self.slide_offset.pack(padx=5, pady=10,side=LEFT)
        self.slide_repeat.pack(padx=5, pady=10,side=LEFT)
        self.importB.pack(padx=50, pady=20)
        self.exportB.pack(padx=50, pady=20)
        self.frame.pack()

    def importImg(self):
        self.filename =  filedialog.askopenfilename(initialdir = "/home/armansansd",title = "Select file",filetypes = (("jpeg files","*.jpg"),("all files","*.*")))
        global path
        path = self.filename
        self.newWindow = tk.Toplevel(self.master)
        self.app = imageBox(self.newWindow)

    def save(self):
        file = filedialog.asksaveasfile(mode='w', defaultextension=".jpg")
        if file:
            im.save(file, "JPEG")


    def sstutter(self, master):
        height = int(round((self.slide_height.get()*h)/100))
        offset = int(round((self.slide_offset.get()*h)/100))
        repeat = int(round((self.slide_repeat.get()*(h/(height+offset)))/100))
        # repeat = int(round((self.slide_repeat.get()*height)/100))
        global im
        im = Image.open(path)
        box = (0, offset, width, height+offset)
        for i in range(repeat):
            region = im.crop(box)
            position = (0,(height+offset)*i)
            im.paste(region, position)

        imageBox.updatePic(self, im)

#https://github.com/HarowitzBlack/Image-Editor-Python-tkinter/blob/master/imageEd/imged.py
# line 174

class imageBox:
    def __init__(self, master):
        try:

            self.master = master
            self.DrawCanvas = Canvas(self.master,width = SIZE[0],height = SIZE[1])

            global canvas
            canvas = self.DrawCanvas

            self.img = Image.open(path)
            self.imgTk = ImageTk.PhotoImage(self.img)
            global h
            h = self.img.size[1]
            global width
            width = self.img.size[0]

            canvas.config(width=width+10, height=h+10)
            global image_on_canvas
            image_on_canvas = self.DrawCanvas.create_image((width+10)/2,(h+10)/2,image=self.imgTk)


            self.DrawCanvas.pack()
        except:
            pass

    def updatePic(self, pic):
        self.imgTk = ImageTk.PhotoImage(pic)
        canvas.itemconfig(image_on_canvas, image=self.imgTk)

def main():
    root = tk.Tk()
    app = toolbar(root)
    root.mainloop()

if __name__ == '__main__':
    main()
